from vunit import VUnit

# Create VUnit instance by parsing command line arguments
vu = VUnit.from_argv()

# Create libraries and add sources
uut_lib = vu.add_library("uut_lib")
uut_lib.add_source_files("src/*.vhd")
tb_lib = vu.add_library("tb_lib")
tb_lib.add_source_files("tests/*.vhd")

# Create Configs
tb_entity = tb_lib.entity("tb_uut")
for i in (4, 8, 16, 32):
    tb_entity.add_config(
        name="Data Width {}".format(i),
        generics={'DATA_WIDTH': i}
    )

vu.main()
